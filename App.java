/*
 * Trabalho de Programa��o Orientada a Objetos
 * Professor: Bernardo Copstein
 * Alunos: Arthur Testa, Matheus Go�s, Felipe Hamester
 *  
 * */
 

import java.util.*;

public class App{
    private CadastroVagoes cv;
    private CadastroLocomotivas c1;
    private CadastroComposicoes cc;
    private Scanner s = new Scanner(System.in);
    
    public App(){
        cv = new CadastroVagoes();
        cv.carrega();
        c1 = new CadastroLocomotivas();
        c1.carrega();
        cc = new CadastroComposicoes();
        cc.carrega();
    }


    public void fim(){
        cv.persiste();
        c1.persiste();
        cc.persiste();
        System.exit(0);
    }

    public void apresentaLocomotivasDisponiveis(){
        for(int i =0; i<c1.qtdade();i++){
            Locomotiva aux = c1.getPorPosicao(i);
                if(aux.getIdentificador() == -1){
                    System.out.println(aux);
                }
        }

    }
    
    public void apresentaVagoesDisponiveis() {
        for(int i =0; i<cv.qtdade();i++){
            Vagao vagAux = cv.getPorPosicao(i);
                if(vagAux.getIdentificador() == -1){
                    System.out.println(vagAux);
                }
            
        }

    } 

    
     
    public void criaComposicao(){
    		System.out.println("Qual o id do novo trem?");
        int id = s.nextInt();
        Composicao trem = new Composicao(id);
        apresentaLocomotivasDisponiveis();
        System.out.println("Insira o id da locomotiva");
        int loc = s.nextInt();
        if(c1.getPorId(id) != null) {
        Locomotiva aux = c1.getPorId(loc);
            if(aux == null || aux.getIdentificador() != -1){
                System.out.println("locomotiva indisponível");
            }
            else {
            	Locomotiva locomAux = new Locomotiva(loc, 500, 10);
                trem.engataLocomotiva(aux);
                cc.cadastra(trem);
                System.out.println("Trem cadastrado");
                this.menu();
        }
        }
    }

    public void editaComposicao(int id){
    	if(cc.getPorIdentificador(id) != null) {
        Composicao trem = cc.getPorIdentificador(id);

    	System.out.println("Escolha uma op��o: 1) Inserir Locomotiva /n 2) Inserir Vag�o /n 3) Remover ultimo elemento /n 4) Listar Locomotivas Livres /n 5) Encerrar edi��o" );
    	
    	int op = s.nextInt();	
    		switch(op) {
    			case 1:
    				System.out.println("Escolha uma das locomotivas abaixo:");
    				apresentaLocomotivasDisponiveis();
    				int loc = s.nextInt();
    		        Locomotiva aux = c1.getPorId(loc);
    		      
    		            if(aux == null || aux.getIdentificador() != -1){
    		                System.out.println("locomotiva indisponível");
    		            }
    		        trem.engataLocomotiva(aux);
    		        System.out.println("Locomotiva" + aux.getIdentificador() + "engatada com sucesso � composi��o " + aux.getComposicao());	
    			break;
    			
    			case 2: 
    				System.out.println("Escolha uma dos vag�es abaixo:");
    			 	apresentaVagoesDisponiveis();
    				int vagao = s.nextInt();
    		        Vagao vagaoAux = cv.getPorId(vagao);
    		            if(vagaoAux == null || vagaoAux.getComposicao() != -1){
    		                System.out.println("locomotiva indisponível");
    		            }
     		        trem.engataVagao(vagaoAux);
    		        System.out.println("Vag�o" + vagaoAux.getIdentificador() + "engatado com sucesso � composi��o " + vagaoAux.getComposicao());	
    			break;
    			
    			case 3:
    					
    				if(trem.getQtdadeVagoes() != 0) {
    					trem.desengataVagao();
    				}
    				else trem.desengataLocomotiva();
    			
    			break;
    			
    			case 4:
    				apresentaLocomotivasDisponiveis();
    				editaComposicao(id);
    			break;
    			
    			case 5:
    				menu();
    			break;
    			
    			default: System.out.println("Op��o incorreta"); editaComposicao(id);
    		}
    	}
    		else {
    			System.out.println("Composi��o n�o encontrada");
    			editaComposicao(id);
    		}
 
    	}
 
    public void menu(){
        while(true){
        	System.out.println("Selecione uma op��o");
        	System.out.println("1)Criar composi��o  2) Editar Composi��o 3) Desfazer Composi��o 4) Listar composi��es 5) Encerrar programa");
            int op = s.nextInt();
            switch(op){ 
                case 1:
                criaComposicao();
                break;

                case 2:
                System.out.println("Insira o identificador da composicao");
                int idComposicao = s.nextInt();
                editaComposicao(idComposicao);
                break;

                case 3:
                	
             	System.out.println("Insira o identificador da composicao");
                idComposicao = s.nextInt();
                Composicao trem = cc.getPorIdentificador(idComposicao);
                trem.desengataTodasLocomotivas();
                trem.desengataTodosVagoes();
                break;
                
                case 4:
                cc.carrega();
                cc.listaTodasComposicoes();
                break;
                
                case 5:
                fim();
                break;
                
                default: System.out.println("Erro na opcao");

            }
        }
    }

    public static void main(String args[]){ 

        App app = new App();
        app.menu();

    }   
}